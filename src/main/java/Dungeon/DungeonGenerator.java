package Dungeon;

import Rooms.Room;
import tools.Directions;
import tools.PlayerUtils;
import tools.RoomState;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class DungeonGenerator {

  private Dungeon dungeon;

  private int maxSteps;
  private int steps = 0;

  private int maxX;
  private int maxY;

  private int centerX;
  private int centerY;

  int playerX;
  int playerY;

  int playerHealth;
  int heals;
  int monsterHealth;
  boolean playerBlock;
  boolean monsterBlock;

  private int keys;

  public DungeonGenerator(int maxX, int maxY, int keys) {
    this.maxX = maxX; this.maxY = maxY; this.keys = keys;
    dungeon = new Dungeon(maxX, maxY);

    // finds center coordinates, and finds max recursion
    centerX = maxX / 2;
    centerY = maxY / 2;
    maxSteps = (centerX > centerY)? centerX : centerY;

    playerX = centerX;
    playerY = centerY;

    playerHealth = 100;
    monsterHealth = 100;
    heals = 2;
    playerBlock = false;
    monsterBlock = false;

    createDungeon();
    placeKeys();
  }

  private void createDungeon() {
    // creates start room
    dungeon.dungeon[centerX][centerY] = new Room();
    ((Room) dungeon.dungeon[centerX][centerY]).openRoom();
    ((Room) dungeon.dungeon[centerX][centerY]).start = true;

    // begin building the dungeon
    createRoom(centerX, centerY + 1, Directions.SOUTH);

    if (!dungeon.isRoom(centerX - 1, centerY))
      createRoom(centerX - 1, centerY, Directions.EAST);
    else
      ((Room) dungeon.dungeon[centerX - 1][centerY]).east = true;

    if (!dungeon.isRoom(centerX, centerY - 1))
      createRoom(centerX, centerY - 1, Directions.NORTH);
    else
      ((Room) dungeon.dungeon[centerX][centerY - 1]).north = true;

    if (!dungeon.isRoom(centerX + 1, centerY))
      createRoom(centerX + 1, centerY, Directions.WEST);
    else
      ((Room) dungeon.dungeon[centerX + 1][centerY]).west = true;
  }

  private void createRoom(int x, int y, Directions d) {
    // if out of bounds, exit
    if (x >= maxX || x < 0 || y >= maxY || y < 0)
      return;

    // if recursions met, create room, and exit
    if (steps == maxSteps) {
      dungeon.dungeon[x][y] = new Room();
      ((Room) dungeon.dungeon[x][y]).setEnterDoor(d);

    // if recursions not met, generate doors
    } else if (steps < maxSteps) {
      steps++;
      dungeon.dungeon[x][y] = new Room();
      ((Room) dungeon.dungeon[x][y]).setEnterDoor(d);

      // if room is not adjacent to wall or room, get chance to generate door and room
      // north
      if (y != maxY - 1 && d != Directions.NORTH) {
        if (!dungeon.isRoom(x, y + 1)) {
          if (getDoorChance()) {
            ((Room) dungeon.dungeon[x][y]).north = true;
            createRoom(x, y + 1, Directions.SOUTH);
          }
        }
      }

      // west
      if (x != 0 && d != Directions.WEST) {
        if (!dungeon.isRoom(x - 1, y)) {
          if (getDoorChance()) {
            ((Room) dungeon.dungeon[x][y]).west = true;
            createRoom(x - 1, y, Directions.EAST);
          }
        }
      }
      // south
      if (y != 0 && d != Directions.SOUTH) {
        if (!dungeon.isRoom(x, y - 1)) {
          if (getDoorChance()) {
            ((Room) dungeon.dungeon[x][y]).south = true;
            createRoom(x, y - 1, Directions.NORTH);
          }
        }
      }
      // east
      if (x != maxX - 1 && d != Directions.EAST) {
        if (!dungeon.isRoom(x + 1, y)) {
          if (getDoorChance()) {
            ((Room) dungeon.dungeon[x][y]).east = true;
            createRoom(x + 1, y, Directions.WEST);
          }
        }
      }

    }
    steps--;
  }

  private void placeKeys() {
    int count = 0;

    while (count != keys) {
      int x = ThreadLocalRandom.current().nextInt(0, maxX);
      int y = ThreadLocalRandom.current().nextInt(0, maxY);
      if (dungeon.isRoom(x, y) && !dungeon.hasItem(x, y) && x != centerX && y != centerY) {
        dungeon.placeItem(x, y);
        count++;
      }
    }
  }

  // get chance to create door
  private boolean getDoorChance() {
    return ThreadLocalRandom.current().nextBoolean();
  }

  public List<Directions> getDirections() {
    List<Directions> d = new ArrayList<>();
    Room r = (Room) dungeon.dungeon[playerX][playerY];

    if (r.north) d.add(Directions.NORTH);
    if (r.south) d.add(Directions.SOUTH);
    if (r.east) d.add(Directions.EAST);
    if (r.west) d.add(Directions.WEST);
    return d;
  }

  public RoomState getRoomState() {
    return ((Room) dungeon.dungeon[playerX][playerY]).state;
  }

  public void defeatMonster() {
    if (getRoomState() == RoomState.MONSTERITEM)
      ((Room) dungeon.dungeon[playerX][playerY]).state = RoomState.ITEM;
    else
      ((Room) dungeon.dungeon[playerX][playerY]).state = RoomState.NOTHING;
  }

  public void takeKey() {
    ((Room) dungeon.dungeon[playerX][playerY]).state = RoomState.NOTHING;
    keys--;
  }

  public int getKeyCount() {
    return keys;
  }

  public boolean moveNorth() {
    if (getDirections().contains(Directions.NORTH)) {
      playerY++;
      return true;
    } else return false;

  }

  public boolean moveSouth() {
    if (getDirections().contains(Directions.SOUTH)) {
      playerY--;
      return true;
    } else return false;
  }

  public boolean moveWest() {
    if (getDirections().contains(Directions.WEST)) {
      playerX--;
      return true;
    } else return false;
  }

  public boolean moveEast() {
    if (getDirections().contains(Directions.EAST)) {
      playerX++;
      return true;
    } else return false;
  }

  public String getPlayerHealth() {
    return PlayerUtils.healthString(playerHealth);
  }

  public String getMonsterHealth() {
    return PlayerUtils.healthString(monsterHealth);
  }

  public int getHeals() {
    return heals;
  }

  public void resetHealth() {
    playerHealth = 100;
    monsterHealth = 100;
    heals = 100;
  }

  public int playerAttackMonster() {
    int hit;
    if (monsterBlock) {
      hit = PlayerUtils.hitBlocked();
      monsterHealth -= hit;
      if (monsterHealth < 0) monsterHealth = 0;
      monsterBlock = false;
      return hit;
    } else {
      hit = PlayerUtils.hit();
      monsterHealth -= hit;
      if (monsterHealth < 0) monsterHealth = 0;
      return hit;
    }
  }

  public int monsterAttackPlayer() {
    int hit;
    if (playerBlock) {
      hit = PlayerUtils.hitBlocked();
      playerHealth -= hit;
      if (playerHealth < 0) playerHealth = 0;
      playerBlock = false;
      return hit;
    } else {
      hit = PlayerUtils.hit();
      playerHealth -= hit;
      if (playerHealth < 0) playerHealth = 0;
      return hit;
    }
  }

  public void playerBlock() {
    playerBlock = true;
  }

  public void monsterBlock() {
    monsterBlock = true;
  }

  public void heal() {
    if (heals > 0) {
      playerHealth += 25;
      heals--;
    }
  }

  public boolean canHeal() {
    return heals > 0;
  }

  public boolean isPlayerDead() {
    return playerHealth == 0;
  }

  public boolean isMonsterDead() {
    return monsterHealth == 0;
  }

  @Override
  public String toString() {
    return dungeon.toString();
  }

  // creates string for discord of dungeon
  public String toDiscord() {
    return dungeon.toDiscord(playerX, playerY);
  }

  public static void main(String[] args) {
    DungeonGenerator dg = new DungeonGenerator(20, 10, 4);
    System.out.println(dg);
    System.out.println();
    System.out.println(dg.toDiscord());
    System.out.println(dg.getPlayerHealth());
  }
}


