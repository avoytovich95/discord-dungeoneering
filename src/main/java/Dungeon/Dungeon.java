package Dungeon;

import Rooms.Cell;
import Rooms.Room;
import tools.RoomState;

public class Dungeon {

  private int maxX;
  private int maxY;
  Cell[][] dungeon;

  public Dungeon(int maxX, int maxY) {
    this.maxX = maxX; this.maxY = maxY;
    dungeon = new Cell[maxX][maxY];
    for (int x = 0; x < maxX; x++) {
      for (int y = 0; y < maxY; y++) {
        dungeon[x][y] = new Cell();
      }
    }
  }

  boolean isRoom(int x, int y) {
    return dungeon[x][y] instanceof Room;
  }

  boolean hasItem(int x, int y) {
    return ((Room) dungeon[x][y]).state == RoomState.ITEM ||
        ((Room) dungeon[x][y]).state == RoomState.MONSTERITEM;
  }

  void placeItem(int x, int y) {
    if (((Room) dungeon[x][y]).state == RoomState.NOTHING)
      ((Room) dungeon[x][y]).state = RoomState.ITEM;
    else if (((Room) dungeon[x][y]).state == RoomState.MONSTER)
      ((Room) dungeon[x][y]).state = RoomState.MONSTERITEM;
  }

  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    for (int y = maxY - 1; y >= 0; y--) {
      for (int x = 0; x < maxX; x++)
        str.append(dungeon[x][y]);
      str.append("\n\n");
    }
    return str.toString();
  }

  public String toDiscord(int playerX, int playerY) {
    StringBuilder str = new StringBuilder();
    for (int y = maxY - 1; y >= 0; y--) {
      for (int x = 0; x < maxX; x++)
        if (x == playerX && y == playerY)
          str.append("X");
        else
          str.append(dungeon[x][y].toDiscord());

      str.append("\n");
    }
    return str.toString();
  }


}
