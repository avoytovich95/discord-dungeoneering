package Rooms;

import tools.Directions;
import tools.RoomState;

import java.util.concurrent.ThreadLocalRandom;

public class Room extends Cell {

  public boolean north;
  public boolean south;
  public boolean east;
  public boolean west;

  public boolean start = false;

  public RoomState state;

  public Room() {
    super();

    if (ThreadLocalRandom.current().nextInt(1, 101) < 25)
      state = RoomState.MONSTER;
    else state = RoomState.NOTHING;
  }

  public void openRoom() {
    north = true;
    south = true;
    west = true;
    east = true;
  }

  public void setEnterDoor(Directions d) {
    switch (d) {
      case NORTH: north = true; break;
      case SOUTH: south = true; break;
      case EAST: east = true; break;
      case WEST: west = true; break;
    }
  }


  @Override
  public String toString() {
    switch (state) {
      case ITEM: return "[$]";
      case MONSTER: return "[X]";
      case MONSTERITEM: return "[!]";
      case NOTHING: return "[ ]";
      default: return "[ ]";
    }
  }

  public String toDiscord() {
    if (start)
      return "⌂";

    else if (north && south && east && west)
      return "╬";

    else if (north && west && east)
      return "╩";
    else if (north && east && south)
      return "╠";
    else if (west && south && east)
      return "╦";
    else if (north && west && south)
      return "╣";

    else if (north && east)
      return "╚";
    else if (east && south)
      return "╔";
    else if (west && south)
      return "╗";
    else if (west && north)
      return "╝";

    else if (north && south)
      return "║";
    else if (west && east)
      return "═";

    else if (north)
      return "╨";
    else if (south)
      return "╥";
    else if (east)
      return "╞";
    else if (west)
      return "╡";

    else return "∙";
  }
}
