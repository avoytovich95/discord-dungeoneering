package tools;

import java.util.concurrent.ThreadLocalRandom;

public class PlayerUtils {

  public static int hit() {
    return ThreadLocalRandom.current().nextInt(1, 26);
  }

  public static int hitBlocked() {
    return ThreadLocalRandom.current().nextInt(0, 11);
  }

  public static Moves getMonsterMove() {
    if (ThreadLocalRandom.current().nextInt(1, 101) < 75)
      return Moves.ATTACK;
    else return Moves.BLOCK;
  }

  public static String healthString(int hp) {
    if (hp == 100)
      return "████100███";

    int r = hp % 10;
    int d = hp / 10;
    StringBuilder str = new StringBuilder();
    for (int i = 0; i < d; i++) {
      str.append("█");
    }
    if (r > 6)
      str.append("▓");
    else if (r > 3)
      str.append("▒");
    else
      str.append("░");
    while (str.length() != 10)
      str.append("░");

    String num = "";
    if (hp < 10)
      num += "0" + hp;
    else
      num += hp;
    String finalStr = str.toString();
    finalStr = finalStr.substring(0, 4) + num + finalStr.substring(6);
    return finalStr;
  }

  public static void main(String[] args) {
    System.out.println(healthString(42));
  }
}
