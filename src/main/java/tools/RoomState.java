package tools;

public enum RoomState {
  NOTHING,
  ITEM,
  MONSTER,
  MONSTERITEM
}
