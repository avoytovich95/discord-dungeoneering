package DungeonMaster;

import Dungeon.DungeonGenerator;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.managers.GuildController;
import net.dv8tion.jda.core.requests.restaction.ChannelAction;
import tools.Directions;
import tools.Moves;
import tools.PlayerUtils;
import tools.RoomState;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class VoteListener extends ListenerAdapter {
    private ArrayList<Member> users = new ArrayList<>();
    private ArrayList<Member> voted = new ArrayList<>();
    private Map<String, Integer> votes = new HashMap<>();
    private boolean voting = false;
    private boolean gameStart = false;
    private boolean movement = true;

    private DungeonGenerator dg = new DungeonGenerator(20, 10, 4);

    private void showInstructions(Guild g) {
        StringBuilder sb = new StringBuilder();
        if(movement) {
            sb.append(dg.toDiscord());
        }
        else {
            sb.append("Player Health\n").append(dg.getPlayerHealth()).append("\nMonster Health\n").append(dg.getMonsterHealth()).append("\nHeals: ").append(dg.getHeals());
        }
        sb.append("\n|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|");
        sb.append("\nKeys Remaining: ").append(dg.getKeyCount()).append("\nVote now!")
                .append("\n|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|").append("\n**__Choices:__**");
        if(movement) {
            dg.getDirections().forEach(d -> sb.append(" !").append(d.toString().toLowerCase()));
        }
        else {
            sb.append(" !attack").append(" !block");
            if(dg.canHeal()) sb.append(" !heal");
        }
        g.getTextChannelsByName("za_dungeon", true).get(0).sendMessage(sb.toString()).queue();
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Guild guild = event.getGuild();
        GuildController controller = event.getGuild().getController();
        if(!event.getAuthor().isBot()) {
            String[] cmd = event.getMessage().getContentRaw().split(" ");
            switch(cmd[0]) {
                case "!start": {
                    if(!gameStart) {
                        gameStart = true;
                        System.out.println(dg.toString());
                        controller.createRole().setName("player").queue();

                        users.add(event.getMember());

                        event.getMessage().getChannel().sendMessage(event.getMember().getAsMention() +
                                " has started a new game! To join the game, type \"!join\".").queue();
                        gameStart = true;

                        controller.createCategory("GAME").queueAfter(10, TimeUnit.SECONDS, (v) -> {
                            // TODO make flag for timer after 10 seconds ends
                            Category cat = guild.getCategoriesByName("GAME", true).get(0);
                            ChannelAction chAct = cat.createTextChannel("za_dungeon")
                                    .addPermissionOverride(guild.getRolesByName("player", true).get(0),
                                            Permission.ALL_TEXT_PERMISSIONS, Permission.ALL_PERMISSIONS)
                                    .addPermissionOverride(guild.getPublicRole(), Permission.MESSAGE_WRITE.getRawValue(),
                                            Permission.MESSAGE_READ.getRawValue());
                            // TODO add reaction permissions
                            chAct.queue((x) -> {
                                for(Member user : users) {
                                    controller.addRolesToMember(user, guild.getRolesByName("player", true)).queue();
                                }

                                // -- START SETUP -- //
                                voted.clear();
                                votes.clear();
                                votes.put("north", 0);
                                votes.put("south", 0);
                                votes.put("east", 0);
                                votes.put("west", 0);
                                // -- END SETUP -- //

                                showInstructions(guild);

                                guild.getTextChannelsByName("za_dungeon", true).get(0).sendMessage("|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|").queue();
                                guild.getTextChannelsByName("za_dungeon", true).get(0).sendMessage("Time's Up!").queueAfter(10, TimeUnit.SECONDS, (t) -> voting = false);
                            });
                        });
                    }
                    break;
                }
                case "!join": {
                    if(gameStart) {
                        if(!users.contains(event.getMember())) {
                            users.add(event.getMember());
                        }
                        else {
                            event.getMessage().getChannel().sendMessage("You have already joined the game, " +
                                    event.getMember().getAsMention()).queue();
                        }
                    }
                    else {
                        event.getMessage().getChannel().sendMessage("A game has not been started.").queue();
                    }
                    break;
                }
                case "!north": {
                    if(voting && !voted.contains(event.getMember()) && movement) {
                        voted.add(event.getMember());
                        votes.put("north", votes.get("north") + 1);
                    }
                    break;
                }
                case "!south": {
                    if(voting && !voted.contains(event.getMember()) && movement) {
                        voted.add(event.getMember());
                        votes.put("south", votes.get("south") + 1);
                    }
                    break;
                }
                case "!east": {
                    if(voting && !voted.contains(event.getMember()) && movement) {
                        voted.add(event.getMember());
                        votes.put("east", votes.get("east") + 1);
                    }
                    break;
                }
                case "!west": {
                    if(voting && !voted.contains(event.getMember()) && movement) {
                        voted.add(event.getMember());
                        votes.put("west", votes.get("west") + 1);
                    }
                    break;
                }
                case "!attack": {
                    if(voting && !voted.contains(event.getMember()) && !movement) {
                        voted.add(event.getMember());
                        votes.put("attack", votes.get("attack") + 1);
                    }
                    break;
                }
                case "!block": {
                    if(voting && !voted.contains(event.getMember()) && !movement) {
                        voted.add(event.getMember());
                        votes.put("block", votes.get("block") + 1);
                    }
                    break;
                }
                case "!heal": {
                    if(voting && !voted.contains(event.getMember()) && !movement) {
                        voted.add(event.getMember());
                        votes.put("heal", votes.get("heal") + 1);
                    }
                    break;
                }
                case "!cleanup": {
                    List<TextChannel> chList = guild.getTextChannelsByName("za_dungeon", true);
                    if(chList.size() > 0) {
                        TextChannel ch = chList.get(0);
                        ch.delete().complete();
                    }
                    List<Category> catList = guild.getCategoriesByName("GAME", true);
                    if(catList.size() > 0) {
                        Category cat = catList.get(0);
                        cat.delete().complete();
                    }
                    List<Role> roles = guild.getRolesByName("player", true);
                    if(roles.size() > 0) {
                        Role r = roles.get(0);
                        r.delete().complete();
                    }
                    break;
                }
                default: {
                    System.out.println("Unrecognized command " + cmd[0]);
                    event.getMessage().getChannel().sendMessage("Unrecognized command: " + cmd[0]).queue();
                    break;
                }
            }
        }
        if(event.getAuthor().isBot()) {
            if(event.getMessage().getContentRaw().equals("Time's Up!")) {
                String result = Collections.max(votes.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();
                if(movement) {
                    guild.getTextChannelsByName("za_dungeon", true).get(0).sendMessage("Moved " + result).queue();
                    if(result.equals("north")) dg.moveNorth();
                    else if(result.equals("south")) dg.moveSouth();
                    else if(result.equals("east")) dg.moveEast();
                    else if(result.equals("west")) dg.moveWest();

                    if(dg.getRoomState() == RoomState.MONSTER || dg.getRoomState() == RoomState.MONSTERITEM)
                        movement = false;
                }
                else {
                    guild.getTextChannelsByName("za_dungeon", true).get(0).sendMessage("Player Action: " + result + "ed").queue();
                    if(result.equals("attack")) dg.playerAttackMonster();
                    else if(result.equals("block")) dg.playerBlock();
                    else if(result.equals("heal")) dg.heal();
                    Moves m = PlayerUtils.getMonsterMove();
                    guild.getTextChannelsByName("za_dungeon", true).get(0).sendMessage("Monster Action: " + m.toString().toLowerCase() + "ed").queue();
                    if(m == Moves.ATTACK) dg.monsterAttackPlayer();
                    else if(m == Moves.BLOCK) dg.monsterBlock();
                }
            }
            if(event.getMessage().getContentRaw().contains("Moved ") || event.getMessage().getContentRaw().contains("Player Action: ")) {

                voted.clear();
                votes.clear();

                if(dg.isMonsterDead()) {
                    movement = true;
                    dg.defeatMonster();
                    if(dg.getRoomState() == RoomState.ITEM) dg.takeKey();
                }
                else if(dg.isPlayerDead()) {
                    guild.getTextChannelsByName("za_dungeon", true).get(0).sendMessage("You lost!").queue();
                }

                if(dg.getKeyCount() == 0) {
                    guild.getTextChannelsByName("za_dungeon", true).get(0).sendMessage("You won!").queue();
                }

                if(movement) {
                    if(dg.getDirections().contains(Directions.NORTH)) votes.put("north", 0);
                    if(dg.getDirections().contains(Directions.SOUTH)) votes.put("south", 0);
                    if(dg.getDirections().contains(Directions.EAST)) votes.put("east", 0);
                    if(dg.getDirections().contains(Directions.WEST)) votes.put("west", 0);
                }
                else {
                    votes.put("attack", 0);
                    votes.put("block", 0);
                    if(dg.canHeal()) votes.put("heal", 0);
                }

                showInstructions(event.getGuild());

                voting = true;
                guild.getTextChannelsByName("za_dungeon", true).get(0).sendMessage("Time's Up!").queueAfter(10, TimeUnit.SECONDS, (v) -> voting = false);
            }
            if(event.getMessage().getContentRaw().contains("You lost!")) {
                guild.getTextChannelsByName("general", true).get(0).sendMessage("Good-bye.").queueAfter(30, TimeUnit.SECONDS, (v) -> v.delete().queue());
            }
            if(event.getMessage().getContentRaw().contains("You won!")) {
                guild.getTextChannelsByName("general", true).get(0).sendMessage("Good-bye.").queueAfter(30, TimeUnit.SECONDS, (v) -> v.delete().queue());
            }
            if(event.getMessage().getContentRaw().equals("Good-bye.")) {
                List<TextChannel> chList = guild.getTextChannelsByName("za_dungeon", true);
                if(chList.size() > 0) {
                    TextChannel ch = chList.get(0);
                    ch.delete().complete();
                }
                List<Category> catList = guild.getCategoriesByName("GAME", true);
                if(catList.size() > 0) {
                    Category cat = catList.get(0);
                    cat.delete().complete();
                }
                List<Role> roles = guild.getRolesByName("player", true);
                if(roles.size() > 0) {
                    Role r = roles.get(0);
                    r.delete().complete();
                }
            }
        }
    }
}
