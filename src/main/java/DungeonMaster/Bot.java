package DungeonMaster;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import javax.security.auth.login.LoginException;

public class Bot extends ListenerAdapter {

    public static void main(String[] args) throws LoginException {
        JDA jdaBot = new JDABuilder(args[0]).build();
        jdaBot.addEventListener(new VoteListener());
    }
}
